###################################################################
# Instance module usage
###################################################################
module "instance" {
  source = "./modules/vsi"

  name                      = var.name
  is_provision              = var.is_provision
  private_key               = var.private_key
  passphrase                = var.passphrase
  vpc_id                    = var.vpc_id
  location                  = var.location
  profile                   = var.profile
  ssh_keys                  = var.ssh_keys
  wait_before_delete        = var.wait_before_delete
  action                    = var.action
  force_action              = var.force_action
  primary_network_interface = var.primary_network_interface
  image                     = var.image
  boot_volume               = var.boot_volume
  volumes                   = var.volumes
  auto_delete_volume        = var.auto_delete_volume
  resource_group_id         = var.resource_group_id
  enable_floating_ip        = var.enable_floating_ip
}
